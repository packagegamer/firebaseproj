﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

namespace FireBaseProj
{
    public class UIController : MonoBehaviour
    {
        [SerializeField, Header("スコア")]
        private Text Score;

        [SerializeField, Header("タイマー")]
        private Text Timer;

        [SerializeField,Header("タイマースピード")]
        private float TimeSpeed = 2;

        void Start()
        {
            GameController.Instance.Init();

            GameController.Instance.TimeLimit.Subscribe(_ =>
            {
                Timer.text = "残り時間: " + GameController.Instance.timeLimit.ToString("f0");
            });

            GameController.Instance.Score.Subscribe(_ =>
            {
                Score.text = "スコア: " + GameController.Instance.GetScore();
            });


            this.UpdateAsObservable().Subscribe(_ =>
            {
                GameController.Instance.timeLimit -= Time.deltaTime * TimeSpeed;
                if(GameController.Instance.timeLimit <= 0)
                {
                    GameController.Instance.timeLimit = 0;
                    SceneManager.Instance.SceneChange(2);
                }
            });
        }
    }

}
