﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FireBaseProj
{
    public class SceneChanger : MonoBehaviour
    {
        [SerializeField, Header("シーン切り替え用ボタン")]
        private Button sceneChangerBtn;
        [SerializeField, Header("シーン切り替え番号")]
        private int sceneNum = 0;

        void Start()
        {
            if (SceneManager.Instance != null)
            {
                sceneChangerBtn.onClick.AddListener(() => SceneManager.Instance.SceneChange(sceneNum));
            }
        }
    }

}
