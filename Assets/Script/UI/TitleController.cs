﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;

public class TitleController : MonoBehaviour
{
    [SerializeField]
    private Image titleImage;

    [SerializeField]
    private Image optionImage;

    [SerializeField]
    private Image rankingImage;

    [SerializeField,Header("移動時間")]
    private float MoveDuration = 2;

    void Start()
    {
        InitWindow();

        this.UpdateAsObservable().Subscribe(_ => 
        {

        });
    }

    /// <summary>
    /// オプション画面表示
    /// </summary>
    public void OptionWindow()
    {
        MoveUI(titleImage, 900, 0);
        MoveUI(optionImage, 0, 2);
    }

    /// <summary>
    /// リザルト画面表示
    /// </summary>
    public void ResultWindow()
    {
        MoveUI(titleImage, 900, 0);
        MoveUI(rankingImage, 0, 2);
    }

    /// <summary>
    /// UI初期化
    /// </summary>
    public void InitWindow()
    {
        if (titleImage.rectTransform.anchoredPosition3D.y == 0)
        {
            MoveUI(titleImage, 900, 0);
        }
        else if (rankingImage.rectTransform.anchoredPosition3D.y == 0)
        {
            MoveUI(rankingImage, 900, 0);
        }
        MoveUI(titleImage, 0, 2);
    }

    /// <summary>
    /// UI移動
    /// </summary>
    /// <param name="image"></param>
    /// <param name="endPos"></param>
    public void MoveUI(Image image, float endPos, float delay)
    {
        image.rectTransform.DOAnchorPosY(endPos, MoveDuration).SetDelay(delay);
    }
}
