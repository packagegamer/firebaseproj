﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace FireBaseProj
{
    public class Item : CharaBase
    {
        [SerializeField, Header("回転スピード")]
        private float speed = 1;

        /// <summary>
        /// フィールドの長さ
        /// </summary>
        const float FIELD_LENGTH = 60;

        void Start()
        {
            transform.position = SetPos();

            this.UpdateAsObservable().Subscribe(_ =>
            {
                base.Rot(speed);
            });
        }

        /// <summary>
        /// 非アクティブのとき
        /// </summary>
        private void OnDisable()
        {
            transform.position = SetPos();
        }

        /// <summary>
        /// 当たり判定
        /// </summary>
        /// <param name="col2D"></param>
        private void OnTriggerEnter2D(Collider2D col2D)
        {
            if (col2D.gameObject.CompareTag("Player"))
            {
                ItemController.Instance.itemPooling.Return(gameObject.GetComponent<Item>());
            }
        }

        /// <summary>
        ///ランダム位置設置
        /// </summary>
        /// <returns>位置取得</returns>
        private Vector2 SetPos()
        {
            int[] cnts = new int[2];
            int randomLength = 0;
            
            for (int i = 0; i < 2; i++)
            {
                int num = Random.Range(0, 2);
                //ランダムな位置に設置
                switch (num)
                {
                    case 0:
                        randomLength = (int)Random.Range(-FIELD_LENGTH, 0);
                        break;
                    case 1:
                        randomLength = (int)Random.Range(0, FIELD_LENGTH);
                        break;
                }
                cnts[i] = randomLength;
            }
            
            Vector2 pos = new Vector2(cnts[0], cnts[1]);

            return pos;
        }

    }
}
