﻿using System;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace FireBaseProj
{
    public class ItemController : SingletonMonoBehaviour<ItemController>
    {
        [SerializeField, Header("生成するアイテム")]
        private Item item;

        [SerializeField, Header("アイテム生成数")]
        private int item_Count = 5;

        [HideInInspector]
        public ItemPooling itemPooling;

        private new void Awake()
        {
            isDestroy = false;
            base.Awake();
        }

        private void Start()
        {
            //オブジェクトプールを生成
            itemPooling = new ItemPooling(item);

            //破棄されたときにPoolを解放する
            this.OnDestroyAsObservable().Subscribe(_ => itemPooling.Dispose());

            //あらかじめObjectを作成しておく
            itemPooling.PreloadAsync(item_Count, 1).Subscribe(_ => {
                //Debug.Log(itemPooling.Count);
                //完了したら表示する
                for(int i = 0; i < item_Count; i++)
                {
                    itemPooling.Rent();
                }
             });

            //生成更新処理
            const int minItem = 1;
            this.UpdateAsObservable().Where(_ => ActiveObjCheck(itemPooling.itemList) <= minItem).Subscribe(_ =>
            {
                itemPooling.Rent();
            });
        }

        /// <summary>
        /// アクティブなオブジェクトの数を取得
        /// </summary>
        /// <param name="itemList">判定するオブジェクトリスト</param>
        /// <returns>アクティブなオブジェクトの数</returns>
        private int ActiveObjCheck(System.Collections.Generic.List<Item> itemList)
        {
            int count = 0;
            try
            {
                if (itemList.Count <= 0) return 0;

                foreach (var obj in itemList)
                {
                    if (obj.gameObject.activeSelf) count++;
                    else break;
                }
            }
            catch(Exception ex)
            {
                Debug.LogError(ex.Message);
            }

            return count;
        }
    }

}
