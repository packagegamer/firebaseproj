﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace FireBaseProj
{
    public abstract class CharaBase : MonoBehaviour
    {
        /// <summary>
        /// 移動関数
        /// </summary>
        /// <param name="pos">位置</param>
        /// <param name="duration">時間</param>
        public virtual void Move(Vector3 pos, float duration)
        {
            transform.DOMove(pos, duration);
        }

        /// <summary>
        /// 移動関数(重力)
        /// </summary>
        /// <param name="rigidbody2D">重力オブジェクト</param>
        /// <param name="moveForce">移動方向の力</param>
        public virtual void Move(Rigidbody2D rigidbody2D, Vector2 moveForce)
        {
            rigidbody2D.AddForce(moveForce);
        }

        /// <summary>
        /// パス移動関数
        /// </summary>
        /// <param name="path">パス</param>
        /// <param name="duration">時間</param>
        /// <param name="pathType">パスの種類</param>
        public virtual void PathMove(List<Transform> path, float duration, PathType pathType)
        {
            //パス取得
            Vector3[] posList = new Vector3[path.Count];
            for (int i = 0; i < path.Count; i++)
            {
                posList[i] = path[i].position;
            }

            //パス移動（ループあり）
            Tweener tweener;
            tweener = transform.DOLocalPath(posList, duration, pathType).SetOptions(true);
        }

        /// <summary>
        /// 回転関数
        /// </summary>
        /// <param name="speed">スピード</param>
        public virtual void Rot(float speed)
        {
            transform.Rotate(new Vector3(0, 0, 90) * speed * Time.deltaTime, Space.World);
        }
    }
}
