﻿using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace FireBaseProj
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Player : CharaBase
    {
        private Rigidbody2D rb2D;
        [SerializeField, Header("プレイヤーのスピード")]
        private float speed = 1.0f;


        private void Start()
        {
            rb2D = GetComponent<Rigidbody2D>();

            this.UpdateAsObservable().Subscribe(_ =>
            {
                float v = Input.GetAxis("Vertical") * speed;
                float h = Input.GetAxis("Horizontal") * speed;
                base.Move(rb2D, new Vector2(h, v));
            });
        }
    }

}
