﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class TargetCamera : MonoBehaviour
{
    [SerializeField, Header("カメラ追従ターゲット")]
    private Transform target;
    [SerializeField, Header("追従スピード")]
    private float smoothing = 5f;

    private void Start()
    {
        this.UpdateAsObservable().Subscribe(_ =>
        {
            transform.position = new Vector3(
                target.position.x, target.position.y, transform.position.z);
        });
    }
}
