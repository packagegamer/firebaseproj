﻿using System.Collections.Generic;
using UnityEngine;
using UniRx.Toolkit;

namespace FireBaseProj
{
    /// <summary>
    /// アイテムクラスのオブジェクトプールクラス
    /// </summary>
    public class ItemPooling : ObjectPool<Item>
    {
        private readonly Item m_itemObj;

        [HideInInspector]
        public List<Item> itemList = new List<Item>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="itemObj">アイテムオブジェクト</param>
        public ItemPooling(Item itemObj)
        {
            m_itemObj = itemObj;
        }

        /// <summary>
        /// アイテムのインスタンス生成
        /// </summary>
        /// <returns>アイテムインスタンス</returns>
        protected override Item CreateInstance()
        {
            //新しく生成
            var item = Object.Instantiate(m_itemObj);
            itemList.Add(item);

            return item;
        }

        /// <summary>
        /// アイテムのオブジェクトプールをクリア
        /// </summary>
        /// <param name="instance">アイテムインスタンス</param>
        protected override void OnClear(Item instance)
        {
            base.OnClear(instance);
            itemList.Clear();
        }
    }

}
