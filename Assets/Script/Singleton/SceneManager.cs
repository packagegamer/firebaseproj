﻿namespace FireBaseProj
{
    public class SceneManager : SingletonMonoBehaviour<SceneManager>
    {
        private new void Awake()
        {
            isDestroy = true;
            base.Awake();
        }

        /// <summary>
        /// シーン切り替え用関数
        /// </summary>
        /// <param name="sceneNum">シーン番号</param>
        public void SceneChange(int sceneNum)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneNum);
        }
    }

}
