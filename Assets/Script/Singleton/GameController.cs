﻿using UnityEngine;
using UniRx;

namespace FireBaseProj
{
    public class GameController : SingletonMonoBehaviour<GameController>
    {
        [SerializeField,Header("マスターデータ")]
        private GameStatus gameStatus;

        /// <summary>
        /// 制限時間
        /// </summary>
        public FloatReactiveProperty TimeLimit { get; set; }

        public float timeLimit {
            get { return TimeLimit.Value; }
            set { TimeLimit.Value = value; }
        }

        /// <summary>
        /// スコア
        /// </summary>
        public IntReactiveProperty Score { get; set; }
        public string GetScore() { return Score.Value.ToString(); }

        /// <summary>
        /// ゲームスタートしてるかフラグ
        /// </summary>
        private BoolReactiveProperty GameStart { get; set; }
        public void SetGameStart(bool isStart) { GameStart.Value = isStart; }
        public bool GetGameStart() { return GameStart.Value; }



        /// <summary>
        /// パラメータ初期化
        /// </summary>
        public void Init()
        {
            TimeLimit = new FloatReactiveProperty(gameStatus.Timer);
            Score = new IntReactiveProperty(0);
            GameStart = new BoolReactiveProperty(false);
        }

    }


}
