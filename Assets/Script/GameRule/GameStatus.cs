﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FireBaseProj
{
    [CreateAssetMenu(menuName = "GameStatus", fileName = "GameStatus")]
    public class GameStatus : ScriptableObject
    {
        [Header("制限時間")]
        public int Timer;

    }
}
